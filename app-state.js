let db;
let phasesUpdated;
let PHASE_PLANNED = 0;
let PHASE_RUNNING = 1;
let PHASE_ENDED = 2;

class AppState {
    constructor() {
        // Setup firebase for persistent storage
        console.log('Connecting to database...');
        let firebase = require('firebase');
        firebase.initializeApp({
            serviceAccount: 'conf/voteitup-05b17d061a88.json',
            databaseURL: 'https://voteitup-93d01.firebaseio.com/'
        });
        db = firebase.database();
        console.log('Connected to database. Now reading data');

        // Callback when firebase data arrives
        this.getData().then(d => {
            this._votes = d.votes;
            this._ideas = d.ideas;
            this._messages = d.messages;
            this._currentVoteId = d.currentVoteId;
            this._currentIdeaId = d.currentIdeaId;
            this._currentMessageId = d.currentMessageId;

            console.log('Done loading data. Server is now ready to use.');
        });

        // set firebase reference variables
        this._fbVotesRef = 'voteitup/votes/';
        this._fbMessagesRef = 'voteitup/messages/';
        this._fbIdeasRef = 'voteitup/ideas/';
        this._fbVoteIdRef = 'voteitup/currentVoteId';
        this._fbMessageIdRef = 'voteitup/currentMessageId';
        this._fbIdeaIdRef = 'voteitup/currentIdeaId';
    }

    getData() {
        return new Promise(function (resolve) {
            let dbReference = db.ref('voteitup');
            dbReference.once('value').then(function (snapshot) {
                dbReference.off('value');
                let dbSnapshot = snapshot.val();
                if (dbSnapshot) {
                    let fbVotes = dbSnapshot.votes || [];
                    let fbMessages = dbSnapshot.messages || [];
                    let fbIdeas = dbSnapshot.ideas || [];

                    // map firebase json list to array
                    let votes = Object.keys(fbVotes).map(k => fbVotes[k]);
                    let messages = Object.keys(fbMessages).map(k => fbMessages[k]);
                    let ideas = Object.keys(fbIdeas).map(k => fbIdeas[k]);

                    let currentVoteId = dbSnapshot.currentVoteId || 0;
                    let currentMessageId = dbSnapshot.currentMessageId || 0;
                    let currentIdeaId = dbSnapshot.currentIdeaId || 0;
                    resolve({votes, messages, ideas, currentVoteId, currentMessageId, currentIdeaId});
                } else {
                    // first initialization
                    resolve({
                        votes: [],
                        messages: [],
                        ideas: [],
                        currentVoteId: 0,
                        currentMessageId: 0,
                        currentIdeaId: 0
                    });
                }
            });
        });
    }

    get votes() {
        this.updatePhasesIfNecessary();
        return this._votes;
    }

    get messages() {
        return this._messages;
    }

    get ideas() {
        return this._ideas;
    }

    updatePhasesIfNecessary() {
        if (this._votes && (!phasesUpdated || phasesUpdated < new Date().setHours(0, 0, 0, 0))) {
            // Updating phases
            this._votes.forEach(v => {
                if (v.currentPhase === PHASE_RUNNING) {
                    let votesTo = Date.parse(v.votesTo);

                    // If end of Vote-Phase set to result-phase
                    if (votesTo <= new Date()) {
                        console.log('Updating phase to ended: ', v.name, v.votesTo);
                        v.currentPhase = PHASE_ENDED;

                        this.addSystemMessage('Abstimmung wurde beendet.', v.id);

                        this.updateVote(v);
                    }
                }
                if (v.currentPhase === PHASE_PLANNED) {
                    let ideasTo = Date.parse(v.ideasTo);

                    // If end of Idea-Phase set to running-phase
                    if (ideasTo <= new Date()) {
                        console.log('Updating phase to running: ', v.name, v.ideasTo);
                        v.currentPhase = PHASE_RUNNING;

                        this.addSystemMessage('Abstimmung wurde gestartet.', v.id);

                        this.updateVote(v);
                    }
                }
            });

            phasesUpdated = new Date().setHours(0, 0, 0, 0);
        }
    }

    addVote(newVote, socket) {
        // Update memory
        newVote.id = ++this._currentVoteId;
        this._votes.push(newVote);

        // Update firebase
        db.ref(this._fbVotesRef + newVote.id).set(newVote);
        db.ref(this._fbVoteIdRef).set(this._currentVoteId);

        // Add system message
        this.addSystemMessage(`${newVote.createdBy.name} hat Themengebiet "${newVote.name}" hinzugefügt`,  newVote.id, socket);

        // send confirmation to the original client
        socket.emit('new_vote', newVote);

        // broadcast to other clients
        socket.broadcast.emit('new_vote', newVote);
    }

    updateVote(existingVote, socket) {
        // Update memory
        var voteIndex = this._votes.findIndex(i => {
            return i.id === existingVote.id;
        });

        this._votes[voteIndex] = existingVote;

        // Update firebase
        db.ref(this._fbVotesRef + existingVote.id).set(existingVote);

        if (socket) {
            // send confirmation to the original client
            socket.emit('update_vote', existingVote);

            // broadcast to other clients
            socket.broadcast.emit('update_vote', existingVote);

            // Add system message
            this.addSystemMessage(`${existingVote.createdBy.name} hat Themengebiet "${existingVote.name}" aktualisiert`,  existingVote.id, socket);
        }
    }

    removeVote(vote, socket) {
        this._votes.splice(this._votes.findIndex(v => v.id === vote.id), 1);

        // Update firebase
        db.ref(this._fbVotesRef + vote.id).set(null);

        // send confirmation to the original client
        socket.emit('remove_vote', vote);

        // broadcast to other clients
        socket.broadcast.emit('remove_vote', vote);

        // Add system message
        this.addSystemMessage(`${vote.createdBy.name} hat Themengebiet "${vote.name}" gelöscht`,  vote.id, socket);
    }

    addIdea(voteId, newIdea, socket) {
        // Update memory
        newIdea.id = ++this._currentIdeaId;
        newIdea.upVotes = [];
        newIdea.voteId = voteId;
        this._ideas.push(newIdea);

        // Update firebase
        db.ref(this._fbIdeasRef + newIdea.id).set(newIdea);
        db.ref(this._fbIdeaIdRef).set(this._currentIdeaId);

        // send confirmation to the original client
        socket.emit('new_idea', newIdea);

        // broadcast to other clients
        socket.broadcast.emit('new_idea', newIdea);

        // Add system message
        this.addSystemMessage(`${newIdea.createdBy.name} hat Idee "${newIdea.name}" hinzugefügt`,  voteId, socket);
    }

    updateIdea(existingIdea, socket) {
        // Update memory
        var ideaIndex = this._ideas.findIndex(i => {
            return i.id === existingIdea.id;
        });

        this._ideas[ideaIndex] = existingIdea;

        // Update firebase
        db.ref(this._fbIdeasRef + existingIdea.id).set(existingIdea);

        // Add system message
        this.addSystemMessage(`${existingIdea.createdBy.name} hat Idee "${existingIdea.name}" aktualisiert`,  existingIdea.id, socket);

        // send confirmation to the original client
        socket.emit('update_idea', existingIdea);

        // broadcast to other clients
        socket.broadcast.emit('update_idea', existingIdea);


    }

    removeIdea(idea, socket) {
        // Update memory
        this._ideas.splice(this._ideas.findIndex(i => i.id === idea.id), 1);

        // Update firebase
        db.ref(this._fbIdeasRef + idea.id).set(null);

        // send confirmation to the original client
        socket.emit('init_ideas', this._ideas);

        // broadcast to other clients
        socket.broadcast.emit('init_ideas', this._ideas);

        // Add system message
        this.addSystemMessage(`${idea.createdBy.name} hat Idee "${idea.name}" gelöscht`,  idea.voteId, socket);
    }

    addMessage(newMessage, socket) {
        // Update memory
        newMessage.id = ++this._currentMessageId;
        newMessage.createdAt = new Date().toISOString();
        this._messages.push(newMessage);

        // Update firebase
        db.ref(this._fbMessagesRef + newMessage.id).set(newMessage);
        db.ref(this._fbMessageIdRef).set(this._currentMessageId);

        if (socket) {
            // send confirmation to the original client
            socket.emit('new_message', newMessage);

            // broadcast to other clients
            socket.broadcast.emit('new_message', newMessage);
        }
    }

    addSystemMessage(message, voteId, socket) {
        this.addMessage({
            message,
            voteId,
            isSystemMessage: true,
            createdBy: {
                name: 'System'
            }
        }, socket);
    }

    voteUp(voteId, ideaId, voter, socket) {
        // Update memory
        let idea = this._ideas.find(i => i.id === ideaId);

        if (idea) {
            let vote = this._votes.find(v => v.id === voteId);

            if (vote && vote.votesPerUser) {
                if (this.getVotesByUserForVote(voter.name, voteId, this._ideas) < vote.votesPerUser) {
                    if (!idea.upVotes) {
                        idea.upVotes = [];
                    }

                    idea.upVotes.push({
                        createdBy: voter,
                        createdAt: new Date().toISOString()
                    });

                    // Update firebase
                    db.ref(this._fbIdeasRef + idea.id + '/upVotes').set(idea.upVotes);

                    // Add system message
                    this.addSystemMessage(`${voter.name} hat gestimmt für "${idea.name}"`,  voteId, socket);
                }
            }

            // send confirmation to the original client
            socket.emit('update_idea', idea);

            // broadcast to other clients
            socket.broadcast.emit('update_idea', idea);
        }
    }

    voteDown(ideaId, voter, socket) {
        // Update memory
        let idea = this._ideas.find(i => i.id === ideaId);

        if (idea && idea.upVotes) {
            let index = idea.upVotes.findIndex(u => u.createdBy.name === voter.name);
            if (index > -1) {
                idea.upVotes.splice(index, 1);

                // Update firebase
                db.ref(this._fbIdeasRef + idea.id + '/upVotes').set(idea.upVotes);

                // Add system message
                this.addSystemMessage(`${voter.name} hat seine Stimme für "${idea.name}" entfernt.`,  idea.voteId, socket);
            }

            // send confirmation to the original client
            socket.emit('update_idea', idea);

            // broadcast to other clients
            socket.broadcast.emit('update_idea', idea);
        }
    }

    getVotesByUserForVote(voterName, voteId, ideas) {
        let countForUser = 0;

        ideas.forEach(i => {
            if (i.voteId === voteId && i.upVotes) {
                i.upVotes.forEach(uv => {
                    if (uv.createdBy.name === voterName) {
                        countForUser++;
                    }
                });
            }
        });

        return countForUser;
    }
}

module.exports = new AppState();

let express = require('express');
let app = express();
app.use(express.static(__dirname + "/"));
let http = require('http');
let server = http.createServer(app);
let io = require('socket.io').listen(server);
let serverPort = process.env.PORT || 4201;
server.listen(serverPort);
io.set("origins", "*:*");

console.log(`Server started on Port ${serverPort}`);

// Setup the application
let appState = require('./app-state');
let socket;

// Define sockets & events
io.on('connection', (s) => {

    socket = s;

    // initial broadcast
    sendInitialStateToClient();

    // events
    socket.on('client_new_vote', newVote => {
        appState.addVote(newVote, socket);
    });
    socket.on('client_new_idea', data => {
        appState.addIdea(data.voteId, data.newIdea, socket);
    });
    socket.on('client_new_message', data => {
        appState.addMessage(data, socket);
    });
    socket.on('client_vote_up', data => {
        appState.voteUp(data.voteId, data.ideaId, data.voter, socket);
    });

    socket.on('client_vote_down', data => {
        appState.voteDown(data.ideaId, data.voter, socket);
    });
    socket.on('client_remove_vote', vote => {
        appState.removeVote(vote, socket);
    });

    socket.on('client_remove_idea', idea => {
        appState.removeIdea(idea, socket);
    });

    socket.on('client_update_idea', idea => {
        appState.updateIdea(idea, socket);
    });

    socket.on('client_update_vote', idea => {
        appState.updateVote(idea, socket);
    })
});

function sendInitialStateToClient() {
    // Send all the initial data to the client
    socket.emit('init_votes', appState.votes);
    socket.emit('init_messages', appState.messages);
    socket.emit('init_ideas', appState.ideas);
}

